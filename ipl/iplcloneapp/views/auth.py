from django.views import View
from django.contrib import messages
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from django.shortcuts import  render,get_object_or_404,redirect
from iplcloneapp.forms.auth import *
from iplcloneapp.models import *
def Logout(request):
    logout(request)
    return redirect('get_matches',2019)
class LoginView(View):
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            return redirect("get_matches",2019)
        login=LoginForm()
        return render(request,template_name="login_page.html",context={'login':login})
    def post(self,request,*args,**kwargs):
        Login=LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user=authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect("get_matches",2019)
        else:
            messages.error(request,'invalid credentials')
        return render(request,template_name="login_page.html",context={'login':Login})

class SignUpView(View):
    def get(self,request,*args,**kwargs):
        signup=SignUpForm()
        return render(request,template_name='signup_page.html',context={'signup':signup})
    def post(self,request,*args,**kwargs):
        signup=SignUpForm(request.POST,request.FILES)
        username=request.POST['username']
        try:
            if User.objects.get(username=username):
                messages.error(request, 'user already exists')
                return redirect("signup_html")
        except:
            if signup.is_valid():



                image=signup.cleaned_data.pop('image')
                user=User.objects.create_user(**signup.cleaned_data)
                user.save()
                ipuser=ipluser()
                ipuser.image=image
                ipuser.user=user
                ipuser.save()

                if ipuser is not None:
                    login(request,user)
                    return redirect("get_matches",2019)
                messages.error(request, 'signup failed')
                return redirect('signup_html')