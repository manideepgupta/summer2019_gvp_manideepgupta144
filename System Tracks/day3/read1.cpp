#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
struct node
{
	int value;
	char formula[100];
};

int strtoint(char *record)
{
	int i = 0;
	int sum = 0;
	while (record[i] != '\0')
	{
		sum *= 10;
		sum += record[i] - '0';
		i++;
	}
	return sum;
}
void strcopy(char a[], char *b)
{
	int i = 0;
	while (b[i] != '\0')
	{
		a[i] = b[i];
		i++;
	}
	a[i] = '\0';
}
void import(char *filename, struct node **mat)
{
	char buffer[1024];
	char *record , *line;
	int i = 0, j = 0;

	FILE *fstream = fopen(filename, "r+");
	if (fstream == NULL)
	{
		printf("\n file opening failed");

	}
	else
	{
		while ((line = fgets(buffer, sizeof(buffer), fstream)) != NULL)
		{
			record = strtok(line, ",");
			while (record != NULL)
			{
				mat[i][j].value = strtoint(record);
				strcopy(mat[i][j++].formula, record);
				record = strtok(NULL, ",");
			}
			i++;
			j = 0;

		}

	}
}

int isnum(char *exp)
{
	int i = 0;
	while (exp[i] != '\0')
	{
		if ((exp[i] >= '0'&&exp[i] <= '9')||exp[i]=='*'||exp[i]=='/'||exp[i]=='%'||exp[i]=='*'||exp[i]=='^'||exp[i]=='+'||exp[i]==')'||exp[i]=='(')
		{
		
			i++;
		}
		else
		{
			return 0;
		}
		
	}
	return 1;
}
char *expansion(char *string, node **mat, int k, int j)
{
	char a[4];
	a[0] = j + 'A';
	a[2] = '\0';
	if (k > 9)
	{
		a[1] = k / 10;
		a[2] = k % 10;
	}
	else
	{
		a[1] = k + '1';
	}
	a[3] = '\0';
	char *res = (char*)malloc(sizeof(char) * 100);
	int i = 0;
	char exp[10];
	int len = 0,in=0;
	while (string[i] != '\0')
	{
		if (string[i] == '%' || string[i] == '/' || string[i] == '-' || string[i] == '+' || string[i] == '^'||string[i]=='*'||string[i]==')')
		{
			exp[len] = '\0';
			if (len > 0)
			{
				if (isnum(exp))
				{
					int f = 0;
					while (exp[f] != '\0')
					{
						res[in++] = exp[f];
						f++;
					}
				}


				else
				{

					int col = exp[0] - 'A';
					int row = exp[1] - '1';
					int f = 0;
					res[in++] = '(';
					while (mat[row][col].formula[f] != '\0')
					{
						res[in++] = mat[row][col].formula[f];
						f++;
					}
					res[in++] = ')';


				}
				
			}
			res[in++] = string[i];
			len = 0;
		}
		else if (string[i] == '(')
			res[in++] = string[i];
		else
		{
			exp[len++] = string[i];
		}
		i++;
	}
	exp[len] = '\0';
	res[in] = string[i];
	if (isnum(exp))
	{
		int f = 0;
		while (exp[f] != '\0')
		{
			res[in++] = exp[f];
			f++;
		}
		res[in++] = exp[f];
	}
	else
	{

		int col = exp[0] - 'A';
		int row = exp[1] - '1';
		int f = 0;
		res[in++] = '(';
		while (mat[row][col].formula[f] != '\0')
		{
			res[in++] = mat[row][col].formula[f];
			f++;
		}
		res[in++] = ')';

	}
	res[in++] = string[i];
	res[in] = '\0';
	if (strstr(res, a))
	{
		
		return NULL;
	}
	if (isnum(res))
	{
		return  res;
	}
	else
	{
		return expansion(res, mat, k, j);
	}
}
int priority(char op)
{
	if (op == '^')
		return 3;
	if (op == '*' || op == '/' || op == '%')
		return 2;
	return 1;
}
int postfix(char *exp[], int len)
{
	int stack[100],top=-1;
	for (int i = 0; i <= len; i++)
	{
		if (exp[i][0] == '/' || exp[i][0] == '%' || exp[i][0] == '*' || exp[i][0] == '^' || exp[i][0] == '+' || exp[i][0] == '-' )
		{
			int b =(stack[top--]);
			int a = stack[top--];
			if (exp[i][0] == '*')
				stack[++top] = a*b;
			else if (exp[i][0] == '/')
				stack[++top] = a / b;
			else if (exp[i][0] == '%')
				stack[++top] = a %b;
			else if (exp[i][0] == '-')
				stack[++top] = a - b;
			else if (exp[i][0] == '+')
				stack[++top] = a + b;
			else if (exp[i][0] == '^')
				stack[++top] = (int)pow((double)a, (double)b);
				
		}
		else
		{
			stack[++top] = strtoint(exp[i]);
		}
	}
	if (top == 0)
		return stack[top];
	return -999999;
}
int evaluation(char *exp)
{
	int i = 0, len = 0;
	char  stack2[100];
	char **stack1 = (char**)malloc(sizeof(char*) * 100);
	for (int i = 0; i < 100; i++)
	{
		stack1[i] = (char*)malloc(sizeof(char) * 100);
	}
	int top1 = -1, top2 = -1;
	char *num = (char*)malloc(sizeof(char) * 10);
	while (exp[i] != '\0')
	{
		if ((exp[i] == '*' || exp[i] == '/' || exp[i] == '%' || exp[i] == '-' || exp[i] == '^' || exp[i] == '+'))
		{
			num[len] = '\0';

			len = 0;
			if (exp[i - 1] != ')')
			{
				strcpy(stack1[++top1], num);
			}
			while (top2 != -1 && stack2[top2] != '(' && (priority(exp[i]) <= priority(stack2[top2])))
			{
				char *a = (char*)malloc(sizeof(char) * 2);
				a[0] = stack2[top2--];
				a[1] = '\0';
				strcpy(stack1[++top1], a);

			}
			stack2[++top2] = exp[i];


		}
		else if (exp[i] == '(')
		{
			stack2[++top2] = exp[i];


		}
		else if (exp[i] == ')')
		{

			if (len != 0)
			{
				num[len] = '\0';
				strcpy(stack1[++top1], num);
			}
			len = 0;
			while (stack2[top2] != '(')
			{
				char *a = (char*)malloc(sizeof(char) * 2);
				a[0] = stack2[top2];
				a[1] = '\0';
				strcpy(stack1[++top1], a);
				top2--;
			}
			top2--;

		}
		else
		{
			num[len++] = exp[i];
		}
		i++;
	}
	num[len] = '\0';

	if (len != 0)

		strcpy(stack1[++top1], num);
	while (top2 != -1)
	{
		char *a = (char*)malloc(sizeof(char) * 2);
		a[0] = stack2[top2--];
		a[1] = '\0';
		strcpy(stack1[++top1], a);

	}
	stack2[++top2] = exp[i];
	
	return postfix(stack1, top1);
}
int evaluate(node cell,int i,int j,node **mat)
{
	char a[4];
	a[0] = j + 'A';
	a[2] = '\0';
	if (i > 9)
	{
		a[1] = i / 10;
		a[2] = i % 10;
	}
	else
	{
		a[1] = i + '1';
	}
	
		
	a[3] = '\0';
	char *result = expansion(cell.formula,mat,i,j);
	if (result != NULL)
	{
		int value = evaluation(result);
		if (value == -999999)
		{
			//printf("invalid expression");
			return 0;
		}
		else
			mat[i][j].value = value;
	}
	else
		return 0;
	return 1;
}
void get(char* str, struct node **mat)
{
	int j = str[0] - 'A';
	int f = 1, i = 0;;
	while (str[f] != '\0')
	{
		i *= 10;
		i += str[f] - '0';
		f++;
	}
	i -=1;
	int flag=evaluate(mat[i][j],i,j,mat);
	if (flag==1)
	printf("%d", mat[i][j].value);
	else
		printf("invalid ");
}
void set(char *str, struct node **mat, char *formula)
{
	int j = str[0] - 'A';
	int i = str[1] - '1';
	strcpy(mat[i][j].formula, formula);
}
void print(struct node **mat)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			int flag = evaluate(mat[i][j], i, j, mat);
			if (flag == 1)
				printf("%d ", mat[i][j].value);
			else
				printf("invalid ");
		}
		printf("\n");
	}
}
void export(char *filename, struct node **mat)
{
	char buffer[1024];
	char *record, *line;
	int i = 0, j = 0;

	FILE *fstream = fopen(filename, "w+");
	if (fstream == NULL)
	{
		printf("\n file opening failed");

	}
	else
	{
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				int flag = evaluate(mat[i][j], i, j, mat);
				int k = -99999;
				if (flag == 1)
					k = mat[i][j].value;

				fprintf(fstream, "%d,", k);
			}
			fprintf(fstream, "\n");
		}
	}
	fclose(fstream);
}
void  lower(char str[])
{
	int i = 0;
	while (str[i] != ' '&&str[i] != '\0')
	{
		if (str[i] >= 'A'&&str[i] <= 'Z')
		{
			str[i] = str[i] - 'A' + 'a';
		}
		i++;
	}
}
int main()
{
	struct node ** mat = (struct node**)malloc(sizeof(struct node*) * 10);
	for (int i = 0; i < 10; i++)
	{
		mat[i] = (struct node*)malloc(sizeof(struct node) * 10);
	}
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			mat[i][j].value = 0;
			
			strcopy(mat[i][j].formula, "0");
		}
	}


	while (1)
	{
		char dummy;
		char *command = (char*)malloc(sizeof(char) * 100);
		char *cd = (char*)malloc(sizeof(char) * 100);
		printf(">");
		scanf("%[^\n]s", command);

		scanf("%c", &dummy);
		lower(command);

		char *record;
       if (strstr(command, "get"))
		{
			strcpy(cd, command);
			int c = 0;
			record = strtok(command, " ");
			while (record != NULL)
			{
				c++;
				record = strtok(NULL, " ");
			}
			if (c > 2)
				printf("invalid function");
			else
			{
				record = strtok(cd, " ");
				record = strtok(NULL, " ");
				get(record, mat);
			}
			printf("\n");
		}
		if (strstr(command, "set"))
		{
			int c = 0;
			record = strtok(command, " ");

			record = strtok(NULL, " ");
			record = strtok(record, "=");
			strcpy(cd, record);
			record = strtok(NULL, "=");
			set(cd, mat, record);
		}

		if (strstr(command, "print"))
		{
			print(mat);
		}
		if (strstr(command, "import"))
		{
			char *record;
			record = strtok(command, " ");
			record = strtok(NULL, " ");
			import(record, mat);
		}
		if (strstr(command, "export"))
		{
			char *record;
			record = strtok(command, " ");
			record = strtok(NULL, " ");
			export(record, mat);
		}
		if (strstr(command, "exit"))
			exit(0);
	}

	return 0;
}