﻿#include<stdio.h>
#include<string.h>
#include<stdlib.h>
struct node
{
	int value;
	
};

int strtoint(char *record)
{
	int i = 0;
	int sum = 0;
	while (record[i] != '\0')
	{
		sum *= 10;
		sum += record[i] - '0';
		i++;
	}
	return sum;
}
void import(char *filename,struct node **mat)
{
	char buffer[1024];
	char *record, *line;
	int i = 0, j = 0;
	
	FILE *fstream = fopen(filename, "r+");
	if (fstream == NULL)
	{
		printf("\n file opening failed");
		
	}
	else
	{
		while ((line = fgets(buffer, sizeof(buffer), fstream)) != NULL)
		{
			record = strtok(line, ",");
			while (record != NULL)
			{
				mat[i][j++].value = strtoint(record);
				record = strtok(NULL, ",");
			}
			i++;
			j = 0;

		}
		
	}
}
void export(char *filename, struct node **mat)
{
	char buffer[1024];
	char *record, *line;
	int i = 0, j = 0;

	FILE *fstream = fopen(filename, "w+");
	if (fstream == NULL)
	{
		printf("\n file opening failed");

	}
	else
	{
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{ 
				int k = mat[i][j].value;
				fprintf(fstream, "%d,", k);
			}
			fprintf(fstream,"\n");
		}
	}
	fclose(fstream);
}
void get(char* str, struct node **mat)
{
	int j = str[0] - 'A';
	int i = str[1] - '1';
	printf("%d", mat[i][j].value);
}
void set(char *str, struct node **mat, int value)
{
	int j = str[0] - 'A';
	int i = str[1] - '1';
	mat[i][j].value = value;
}
void print(struct node **mat)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			printf("%d ", mat[i][j].value);
		}
		printf("\n");
	}
}
void  lower(char str[])
{
	int i = 0;
	while (str[i] != ' '&&str[i]!='\0')
	{
		if (str[i]>='A'&&str[i] <= 'Z')
		{
			str[i]=str[i] - 'A' + 'a';
		}
		i++;
	}
}
int main()
{
	struct node ** mat = (struct node**)malloc(sizeof(struct node*) * 10);
	for (int i = 0; i < 10; i++)
	{
		mat[i] = (struct node*)malloc(sizeof(struct node) * 10);
	}
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			mat[i][j].value = 0;
			
		}
	}

	
	while (1)
	{
		char dummy;
		char command[100],cd[100];
		printf(">");
		scanf("%[^\n]s", command);
		
		scanf("%c", &dummy);
		lower(command); 
		
		char *record;
		if(strstr(command, "get"))
		{
			strcpy(cd, command);
			int c = 0;
			record = strtok(command, " ");
			while (record != NULL)
			{
				c++;
				record=strtok(NULL, " ");
			}
			if (c > 2)
				printf("invalid function");
			else
			{
				record = strtok(cd, " ");
				record = strtok(NULL, " ");
				get(record, mat);
			}
			printf("\n");
		}
		if (strstr(command, "set"))
		{
			int c = 0;
			record = strtok(command, " ");
			
			record = strtok(NULL, " ");
			record = strtok(record, "=");
			strcpy(cd, record);
			record = strtok(NULL, "=");
			set(cd, mat, strtoint(record));
		}

		if (strstr(command, "print"))
		{
			print(mat);
		}
		if (strstr(command, "import"))
		{
			char *record;
			record = strtok(command, " ");
			record= strtok(NULL, " ");
			import(record, mat);
		}
		if (strstr(command, "export"))
		{
			char *record;
			record = strtok(command, " ");
			record = strtok(NULL, " ");
			export(record, mat);
		}
	}
	
	return 0;
}