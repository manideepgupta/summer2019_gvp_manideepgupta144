#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *next, *random;
};
typedef struct node Node;
Node *getnode(int value)
{
	Node* newnode = (Node*)malloc(sizeof(Node));
	newnode->data = value;
	newnode->next = NULL;
	newnode->random = NULL;
	return newnode;

}
struct map
{
	Node *key, *value;
};
Node *search(Node *head, struct map mymap[], int index)
{
	for (int i = 0; i < index; i++)
	{
		if (mymap[i].key == head)
		{
			return mymap[i].value;
		}
	}
	return NULL;
}
Node *clone(node *head)
{
	if (head == NULL)
	{
		return NULL;
	}
	struct map mymap[100];
	int index = 0;
	Node *temp = head;
	while (temp != NULL)
	{
		mymap[index].key = temp;
		mymap[index].value = getnode(temp->data);
		temp = temp->next;
		index++;
	}
	temp = head;
	Node *t = search(head,mymap,index);
	Node *temp1 = t;

	for (int i = 1; i <= index; i++)
	{
		temp1->next = search(temp->next, mymap, index);
		temp1->random = search(temp->random, mymap, index);
		temp1 = temp1->next;
		temp = temp->next;
	}
	return t;

}
void  print(Node *temp)
{
	while (temp != NULL)
	{
		printf("Data = %d ,Random = %d\n", temp->data, temp->random->data);
		temp = temp->next;
	}
}
int main()
{
	Node *start = getnode(1);
	start->next = getnode(2);
	start->next->next = getnode(3);
	start->next->next->next = getnode(4);
	start->next->next->next->next = getnode(5);
	start->random = start->next->next;
	start->next->random = start;
	start->next->next->random = start->next->next->next->next;
	start->next->next->next->random = start->next->next->next->next;
	start->next->next->next->next->random = start->next;
	print(start);
	Node *list = clone(start);
	printf("\n");
	print(list);

	system("pause");
	return 0;
}