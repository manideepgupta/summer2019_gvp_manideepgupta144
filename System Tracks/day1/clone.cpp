#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *next, *random;
};
typedef struct node Node;
Node *getnode(int value)
{
	Node* newnode = (Node*)malloc(sizeof(Node));
	newnode->data = value;
	newnode->next = NULL;
	newnode->random = NULL;
	return newnode;

}
Node *clone(node *head)
{
	if (head == NULL)
		return NULL;
	Node *temp = head;
	Node *temp1 = NULL;
	while (temp != NULL)
	{
		temp1 = getnode(temp->data);
		temp1->next = temp->next;
		temp->next = temp1;
		temp = temp->next->next;
	}
	temp = head;
	while (temp != NULL)
	{
		temp->next->random = temp->random ? temp->random->next : temp->random;
		temp = temp->next->next;
	}
	temp = head;
	Node *t = temp->next;
	temp1 = head->next;
	while (temp != NULL)
	{
		temp->next = temp->next->next;
		
		temp1->next = temp1->next?temp1->next->next:temp1->next; 
		temp = temp->next;
		temp1 = temp1->next;
	}
	return t;

}
void  print(Node *temp)
{
	while (temp != NULL)
	{
		printf("Data = %d ,Random = %d\n", temp->data, temp->random->data);
		temp = temp->next;
	}
}
int main()
{
	Node *start = getnode(1);
	start->next = getnode(2);
	start->next->next = getnode(3);
	start->next->next->next = getnode(4);
	start->next->next->next->next = getnode(5);
	start->random = start->next->next;
	start->next->random = start;
	start->next->next->random = start->next->next->next->next;
	start->next->next->next->random = start->next->next->next->next;
	start->next->next->next->next->random = start->next;
	print(start);
	Node *list = clone(start);
	printf("\n");
	print(list);
	system("pause");
	return 0;
}