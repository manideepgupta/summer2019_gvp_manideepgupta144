#include "filereader.h"
int blockRead(int blockNo, void *buffer)
{
	FILE *fptr = fopen("hardDisk.hdd", "rb+");
	if (fptr == NULL)
	{
		printf("file not found");
		return -1;
	}
	fseek(fptr, blockNo*BLOCK_SIZE, SEEK_SET);
	fread(buffer, BLOCK_SIZE, 1, fptr);
	fclose(fptr);
	return 1;
}
int blockWrite(int blockNo, void *buffer)
{
	FILE *fptr = fopen("hardDisk.hdd", "rb+");
	if (fptr == NULL)
	{
		printf("file not found");
		return -1;
	}
	fseek(fptr, blockNo*BLOCK_SIZE, SEEK_SET);
	fwrite(buffer, BLOCK_SIZE, 1, fptr);
	fclose(fptr);
	return 1;
}
void format()
{
	struct metaData Metadata;
	Metadata.magicNumber = CHECKER;
	Metadata.free_blocks = BLOCKS(DISK_SIZE);
	Metadata.no_of_files = 0;

	for (int i = 0; i < FILES * 5; i++)
	{
		Metadata.bitVector[i] = '0';
	}

	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);

	memcpy(buffer, &Metadata, sizeof(struct metaData));
	blockWrite(0, buffer);
	free(buffer);

}
int fileSize(char *filename)
{
	FILE *fptr = fopen(filename, "rb+");
	if (fptr == NULL)
	{
		printf("file not found");
		return -1;
	}
	fseek(fptr, 0L, SEEK_END);
	int size = ftell(fptr);
	fclose(fptr);
	return size;

}
int findStartBlockNo(char bitvector[])
{

	int i = 1;
	int count = 0;
	while (i < BLOCKS(DISK_SIZE) - 2)
	{
		if (bitvector[i] == '1')
			count = 0;
		else
		{
			bitvector[i] = '1';
			return i;
		}
		i++;

	}
	return -1;
}
void addFileDetails(char *filename, struct metaData metadata, int blocks, int blockno, int length, int list[])
{

	metadata.free_blocks -= blocks;

	strcpy(metadata.fileDetails[metadata.no_of_files + 1].name, filename);
	metadata.fileDetails[metadata.no_of_files + 1].blockno = blockno;
	metadata.fileDetails[metadata.no_of_files + 1].block_count = blocks;
	metadata.fileDetails[metadata.no_of_files + 1].length = length;
	metadata.magicNumber = CHECKER;
	metadata.no_of_files += 1;

	int k = 0;
	for (int i = blockno + 1; blocks>0; i++)
	{

		if (metadata.bitVector[i] == '0')
		{
			list[k++] = i;
			blocks--;
			metadata.bitVector[i] = '1';
		}

	}

	void *buffer1 = (void*)malloc(sizeof(char)*BLOCK_SIZE);
	memcpy(buffer1, &metadata, sizeof(struct metaData));
	blockWrite(0, buffer1);
	free(buffer1);



}

int copy_to_disk(char *source, char *destination)
{
	int length = fileSize(source);
	if (length == -1)
	{
		return -1;

	}
	int blocks = BLOCKS(length);
	struct metaData Metadata;
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	if (blockRead(0, buffer) == -1)
	{
		return -1;
	}
	memcpy(&Metadata, buffer, sizeof(struct metaData));
	int blockno = findStartBlockNo(Metadata.bitVector);
	if (blockno == -1)
	{
		printf("disk size is less\n");
		return -1;
	}
	int list[10000];
	addFileDetails(destination, Metadata, blocks, blockno, length, list);
	int list1[10000];

	memcpy(buffer, list, sizeof(int)*blocks);
	blockWrite(blockno, buffer);

	FILE *fptr = fopen(source, "rb+");
	if (fptr == NULL)
	{
		printf("file not found\n");
		return -1;
	}
	for (int i = 0; i <blocks; i++)
	{

		if (length >= BLOCK_SIZE)
			fread(buffer, BLOCK_SIZE, 1, fptr);
		else
		{
			fread(buffer, length, 1, fptr);
		}
		length -= BLOCK_SIZE;
		blockWrite(list[i], buffer);
	}
	free(buffer);
	fclose(fptr);
	return 1;


}
int copy_from_disk(char *source, char *destination)
{

	int blockno = -1;
	struct metaData Metadata;
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	blockRead(0, buffer);
	memcpy(&Metadata, buffer, sizeof(struct metaData));


	for (int i = 1; i <= Metadata.no_of_files; i++)
	{
		if (strcmp(Metadata.fileDetails[i].name, source) == 0)
		{
			FILE *fptr = fopen(destination, "wb+");
			int length = Metadata.fileDetails[i].length;
			int blocks = BLOCKS(length);
			blockno = Metadata.fileDetails[i].blockno;
			blockRead(blockno, buffer);
			int list[10000];
			memcpy(list, buffer, sizeof(int)*blocks);
			for (int i = 0; i <blocks; i++)
			{
				blockRead(list[i], buffer);
				if (length >= BLOCK_SIZE)
					fwrite(buffer, BLOCK_SIZE, 1, fptr);
				else
				{
					fwrite(buffer, length, 1, fptr);
				}
				length -= BLOCK_SIZE;

			}
			fclose(fptr);
			return 1;
		}
	}
	printf("file not found to copy\n");
	return -1;
}
int deleteFile(char *filename)
{
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	blockRead(0, buffer);
	struct metaData Metadata;
	memcpy(&Metadata, buffer, sizeof(struct metaData));

	if (Metadata.no_of_files == 1)

	{
		format();
		return 1;
	}

	for (int i = 1; i <= Metadata.no_of_files; i++)
	{
		if (strcmp(Metadata.fileDetails[i].name, filename) == 0)
		{
			blockRead(Metadata.fileDetails[i].blockno, buffer);
			int *list = (int*)malloc(sizeof(int)*Metadata.fileDetails[i].block_count);
			memcpy(list, buffer, sizeof(list));
			Metadata.bitVector[Metadata.fileDetails[i].blockno] = '0';
			for (int j = 0; j <Metadata.fileDetails[i].block_count; j++)
				Metadata.bitVector[list[j]] = '0';
			strcpy(Metadata.fileDetails[i].name, Metadata.fileDetails[Metadata.no_of_files].name);
			Metadata.fileDetails[i].length = Metadata.fileDetails[Metadata.no_of_files].length;

			Metadata.fileDetails[i].block_count = Metadata.fileDetails[Metadata.no_of_files].block_count;
			Metadata.fileDetails[i].blockno = Metadata.fileDetails[Metadata.no_of_files].blockno;
			Metadata.no_of_files -= 1;

			memcpy(buffer, &Metadata, sizeof(struct metaData));
			blockWrite(0, buffer);
			free(buffer);
			return 1;
		}
	}
	free(buffer);
	printf("file not found to delete\n");
	return -1;
}

void ls()
{
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	blockRead(0, buffer);
	struct metaData Metadata;
	memcpy(&Metadata, buffer, sizeof(struct metaData));
	for (int i = 1; i <= Metadata.no_of_files; i++)
	{
		printf("%s\n", Metadata.fileDetails[i].name);
	}
	free(buffer);

}
void debug()
{
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	blockRead(0, buffer);
	struct metaData Metadata;
	memcpy(&Metadata, buffer, sizeof(struct metaData));
	printf("free blocks :%d\n", Metadata.free_blocks);
	printf("no_of_files :%d\n", Metadata.no_of_files);
	printf("magicnumber :%d\n", Metadata.magicNumber);


}
int isEqual(char *str1, char *str2)
{
	for (int i = 0; str1[i] != '\0'; i++)
	{
		if (str1[i] != str2[i])
			return 0;
	}
	return 1;
}
void print(void *buffer, int blocksize)
{
	char *buffr = (char*)malloc(sizeof(char)*blocksize);
	memcpy(buffr, buffer, blocksize);
	char *record = strtok(buffr, "\n");
	while (record != NULL)
	{
		printf("%s\n", record);
		record = strtok(NULL, "\n");
	}
}
void readfromfile(char *filename)
{
	void *buffer = malloc(sizeof(char)*BLOCK_SIZE);
	blockRead(0, buffer);
	struct metaData Metadata;
	memcpy(&Metadata, buffer, sizeof(struct metaData));
	for (int i = 1; i <= Metadata.no_of_files; i++)
	{
		if (strcmp(Metadata.fileDetails[i].name, filename) == 0)
		{
			blockRead(Metadata.fileDetails[i].blockno, buffer);
			int *list = (int*)malloc(sizeof(int)*Metadata.fileDetails[i].block_count);
			memcpy(list, buffer, sizeof(list));
			int size = Metadata.fileDetails[i].length;
			for (int j = 0; j < Metadata.fileDetails[i].block_count; j++)
			{
				blockRead(list[j], buffer);
				if (size > BLOCK_SIZE)
					print(buffer, BLOCK_SIZE);
				else
					print(buffer, size);
				size -= BLOCK_SIZE;

			}
		}
	}
}
