from django.shortcuts import render
from django.views import View
from todoapp.models import *
from todoapp.templates import *
# Create your views here.
class ToDoListView(View):
    def get(self,request,*args,**kwargs):
        todolist=Todolist.objects.all()
        return render(request,template_name='ToDoList.html',context={'ToDoList':todolist})
