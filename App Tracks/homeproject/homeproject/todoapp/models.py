from django.db import models

# Create your models here.
class Todolist(models.Model):
    name=models.CharField(max_length=128)
    created=models.DateTimeField()
    def __str__(self):
        return self.name
class Todoitem(models.Model):
    description=models.CharField(max_length=128)
    due_date=models.DateField(default=None)
    completed=models.BooleanField()
    todolist=models.ForeignKey(Todolist,on_delete=models.CASCADE)
    def __str__(self):
        return self.description
