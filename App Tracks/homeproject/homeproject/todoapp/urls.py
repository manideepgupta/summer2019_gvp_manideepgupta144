from django.urls import path
from todoapp.views import todolist


urlpatterns = [
    path('todolist/',todolist.ToDoListView.as_view(),name='todolist_html'),
        ]