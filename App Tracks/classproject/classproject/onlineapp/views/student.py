from django.views import View
from onlineapp.models import *
from django.urls import resolve
from django.shortcuts import  render,get_object_or_404,redirect
from onlineapp.forms.students import *
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
class MarksView(LoginRequiredMixin,View):
    login_url = '/login/'

    def get(self,request,*args,**kwargs):
        marks = MockTest1.objects.values('student__name', 'student__id','total').filter(student__college__acronym=kwargs.get('acronym')).order_by('-total')

        return render(
            request,
            template_name='get_marks.html',
            context={
                'students':marks,
                'college':kwargs.get('acronym'),
                'permissions':request.user.get_all_permissions()
            }
        )

class addStudentView(PermissionRequiredMixin,LoginRequiredMixin,View):
    login_url = '/login/'
    permission_required = ('onlineapp.change_student','onlineapp.add_student','onlineapp.delete_student')
    def get(self,request,*args,**kwargs):
        if resolve(request.path_info).url_name == 'delete_student_html':
            student = Student.objects.get(id=kwargs.get('pk'))
            mocktest = MockTest1.objects.get(student__id=kwargs.get('pk'))
            student.delete()
            mocktest.delete()
            return redirect('collegs_html')

        studentforms = addStudent()
        marksforms = addMarks()
        if resolve(request.path_info).url_name == 'edit_student_html':
            student=Student.objects.get(id=kwargs.get('pk'))
            mocktest=MockTest1.objects.get(student__id=kwargs.get('pk'))
            studentforms=addStudent(instance=student)
            marksforms=addMarks(instance=mocktest)
        return render(request,template_name='add_student_details.html',context={'student':studentforms,'marks':marksforms})

    def post(self,request,*args,**kwargs):
        print('---post method ----')

        if resolve(request.path_info).url_name == 'edit_student_html':
            student = Student.objects.get(id=kwargs.get('pk'))
            mocktest = MockTest1.objects.get(student__id=kwargs.get('pk'))
            studentforms = addStudent(request.POST,instance=student)
            marksforms = addMarks(request.POST,instance=mocktest)
            m=marksforms.save(commit=False)
            m.total = m.problem1 + m.problem2 + m.problem3 + m.problem4
            m.save()
            studentforms.save()
            return redirect('collegs_html')

        studentforms = addStudent(request.POST)
        marksforms = addMarks(request.POST)
        if studentforms.is_valid() and marksforms.is_valid():

            k=studentforms.save(commit=False)
            k.college=College.objects.get(**kwargs)
            k.dob='1998-01-01'
            k.save()

            m= marksforms.save(commit=False)
            m.total=m.problem1+m.problem2+m.problem3+m.problem4
            m.student=k
            m.save()
            #change
            #return redirect('collegs_html')
            return redirect('marks_html',acronym=kwargs.get('acronym'))
