from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from django.views import View
from onlineapp.models import College,Student,MockTest1
from onlineapp.Serializers import *
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
@api_view(['GET', 'POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication,TokenAuthentication))
@permission_classes((IsAuthenticated,))
def college_list(request):
    if request.method == 'GET':
        college = College.objects.all()
        serializer = CollegeSerializer(college, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = CollegeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
@api_view(['GET','PUT','DELETE'])
@authentication_classes((SessionAuthentication, BasicAuthentication,TokenAuthentication))
@permission_classes((IsAuthenticated,))
def college_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        college = College.objects.get(pk=pk)
    except College.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CollegeSerializer(college)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CollegeSerializer(college, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        college.delete()
        return HttpResponse(status=204)
class Studentlist(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication,TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self,request,*args,**kwargs):
        try:
            student=Student.objects.filter(college__id=kwargs.get('cid'))
        except:
            return HttpResponse(status=404)
        serializer=StudentSerializer(student,many=True)
        return Response(serializer.data)

    def post(self,request,*args,**kwargs):
        serializer=StudentSerializer(data=request.data,context={'pk':kwargs.get('cid')})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class StudentDetails(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    def get(self,request,*args,**kwargs):
        try:
            students = Student.objects.filter(college__id=kwargs.get('cid')).get(pk=kwargs.get('sid'))
        except Student.DoesNotExist:
            return HttpResponse(status=404)
        serializer=StudentSerializer(students)
        return Response(serializer.data)
    def put(self,request,*args,**kwargs):
        try:
            students = Student.objects.filter(college__id=kwargs.get('cid')).get(pk=kwargs.get('sid'))
        except Student.DoesNotExist:
            return HttpResponse(status=404)
        serializer=StudentSerializer(students,request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,*args,**kwargs):
        try:
            students = Student.objects.filter(college__id=kwargs.get('cid')).get(pk=kwargs.get('sid'))
        except Student.DoesNotExist:
            return HttpResponse(status=404)
        students.delete()
        return HttpResponse(status=204)
