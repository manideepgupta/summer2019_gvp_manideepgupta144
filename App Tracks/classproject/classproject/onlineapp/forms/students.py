from onlineapp.models import *
from django import forms
class addStudent(forms.ModelForm):
    class Meta:
        model=Student
        exclude=['id','dob','college']
class addMarks(forms.ModelForm):
    class Meta:
        model=MockTest1
        exclude=['student','total']