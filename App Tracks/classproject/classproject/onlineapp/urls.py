from django.urls import path
from onlineapp.views import college,student,login,rest,test,auth
from django.views.decorators.csrf import csrf_exempt


urlpatterns = [
    #path('get_my_college',views.get_my_college),
    #path('get_all_colleges',views.get_all_colleges),
    #path('get_all_colleges2',views.get_all_colleges2),
    #path('get_all_colleges_markslist',views.get_all_colleges_markslinks),
    #path('get_all_marks/<str:college>',views.get_marks_college)
    path('colleges/',college.CollegeView.as_view(),name="collegs_html"),
    #path('get_all_marks/<str:acronym>',college.MarksView.as_view(),name="marks_html"),
    path('add_college/',college.addCollegeView.as_view(),name="add_college_html"),
    path('add_college/<str:acronym>/edit',college.addCollegeView.as_view(),name="edit_college_html"),
    path('add_college/<str:acronym>/delete', college.addCollegeView.as_view(), name="delete_college_html"),
    #path('students/',student.CollegeView.as_view(),name='students_html'),
    path('get_all_marks/<str:acronym>/', student.MarksView.as_view(), name="marks_html"),
    path('add_student/<str:acronym>/',student.addStudentView.as_view(),name='add_student_html'),
    path('logout/',login.Logout,name='logout_html'),
    path('add_student/<int:pk>/edit', student.addStudentView.as_view(), name='edit_student_html'),
    path('add_student/<int:pk>/delete', student.addStudentView.as_view(), name='delete_student_html'),
    path('login/',login.LoginView.as_view(),name='login_html'),
    path('signup/',login.SignUpView.as_view(),name='signup_html'),
    path('api/v1/colleges/', rest.college_list,name='rest_colleges'),
    path('api/v1/colleges/<int:pk>',rest.college_detail,name='rest_college_detail'),
    path('api/v1/colleges/<int:cid>/students',rest.Studentlist.as_view(),name='rest_student_list'),
    path('api/v1/colleges/<int:cid>/students/<int:sid>',rest.StudentDetails.as_view(),name='rest_student_detail'),
    path('test/',test.test_view),
    path('api/v1/auth',auth.CustomAuthToken.as_view(),name='auth_page')
    ]