from openpyxl import load_workbook
from bs4 import BeautifulSoup
import os
import django
import click
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()

from onlineapp.models import *
from os import path
@click.group()
def onlinedb_gro():
    pass
@onlinedb_gro.command()
def importfromexcel():
    wb = load_workbook('students.xlsx')
    sheet1 = wb['Colleges']
    for i in range(2, sheet1.max_row+1):
        row = sheet1[i]
        c = College(name=row[0].value,location=row[2].value,acronym=row[1].value,contact=row[3].value)
        c.save()
    wb = load_workbook('students.xlsx')
    sheet1 = wb['Current']
    for i in range(2, sheet1.max_row + 1):
        row = sheet1[i]
        print(row[1].value)
        c = Student(name=row[0].value,dob='1998-01-01',email=row[2].value,db_folder=row[3].value,dropped_out=True,college=College.objects.get(acronym=row[1].value))
        c.save()

@onlinedb_gro.command()
def importfromhtml():
    with open('ss.html') as fp:
        soup = BeautifulSoup(fp, features="html.parser")
    for i in range(1, len(soup.find_all('tr'))):
        row=soup.find_all('tr')[i].find_all('td')
        if row[1].text.split('_')[2]!='skc':
            c = MockTest1(problem1=int(row[2].text),problem2=int(row[3].text),problem3=(row[4].text),problem4=(row[5].text),total=(row[6].text),student=Student.objects.get(db_folder=row[1].text.split('_')[2]))
            c.save()
onlinedb_gro()
